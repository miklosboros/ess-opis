<?xml version="1.0" encoding="UTF-8"?>
<databrowser>
  <title>Cold box</title>
  <show_legend>true</show_legend>
  <show_toolbar>true</show_toolbar>
  <grid>true</grid>
  <update_period>3.0</update_period>
  <scroll_step>5</scroll_step>
  <scroll>true</scroll>
  <start>-30 minutes</start>
  <end>now</end>
  <archive_rescale>NONE</archive_rescale>
  <foreground>
    <red>0</red>
    <green>0</green>
    <blue>0</blue>
  </foreground>
  <background>
    <red>242</red>
    <green>242</green>
    <blue>242</blue>
  </background>
  <title_font>Liberation Sans|20.0|1</title_font>
  <label_font>Liberation Sans|14.0|1</label_font>
  <scale_font>Liberation Sans|12.0|0</scale_font>
  <legend_font>Liberation Sans|14.0|0</legend_font>
  <axes>
    <axis>
      <visible>true</visible>
      <name>K</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>true</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>8.3525</min>
      <max>11.8175</max>
      <grid>false</grid>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
    </axis>
    <axis>
      <visible>true</visible>
      <name>%</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>true</use_trace_names>
      <right>true</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>32.464999999999996</min>
      <max>53.035000000000004</max>
      <grid>false</grid>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
    </axis>
    <axis>
      <visible>true</visible>
      <name>K bis</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>true</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>74.20800000000001</min>
      <max>82.832</max>
      <grid>false</grid>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
    </axis>
  </axes>
  <annotations>
  </annotations>
  <pvlist>
    <pv>
      <display_name>T2 outlet</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-TE-31459B:Val</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
      </archive>
    </pv>
    <pv>
      <display_name>last exchanger</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-TE-31460A:Val</name>
      <axis>0</axis>
      <color>
        <red>128</red>
        <green>0</green>
        <blue>128</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
      </archive>
    </pv>
    <pv>
      <display_name>dewar inlet HP</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-CV-31469A:Out</name>
      <axis>1</axis>
      <color>
        <red>255</red>
        <green>127</green>
        <blue>0</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
      </archive>
    </pv>
    <pv>
      <display_name>dewar outlet LP</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-CV-33470A:Out</name>
      <axis>1</axis>
      <color>
        <red>128</red>
        <green>128</green>
        <blue>0</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
      </archive>
    </pv>
    <pv>
      <display_name>Top exchanger</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-TE-31401A:Val</name>
      <axis>2</axis>
      <color>
        <red>127</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
      </archive>
    </pv>
  </pvlist>
</databrowser>
