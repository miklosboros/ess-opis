<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>MEBT Vacuum System</name>
  <macros>
    <DIS>Vac</DIS>
    <ROOT>$(ESS_OPIS=/ess-opis)/NON-APPROVED</ROOT>
    <SEC>MEBT</SEC>
    <SUBSEC>010</SUBSEC>
    <WIDGET_ROOT>$(ROOT)/COMMON/DEVICES/vacuum</WIDGET_ROOT>
  </macros>
  <width>1800</width>
  <height>1100</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <text>            MEBT Vacuum System</text>
    <width>1800</width>
    <height>80</height>
    <font>
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color>
      <color name="White" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="Primary Blue" red="0" green="148" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open VGC Trends Action</name>
    <actions>
      <action type="open_display">
        <file>Trending MEBT VGC.bob</file>
        <target>tab</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>Open
VGC Trends</text>
    <x>1100</x>
    <y>10</y>
    <width>150</width>
    <height>60</height>
    <font>
      <font name="GROUP-HEADER" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK-TEXT" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open Diagnostics Action</name>
    <actions>
      <action type="open_display">
        <file>diagnostics.bob</file>
        <target>standalone</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>Open
Diagnostics</text>
    <x>1300</x>
    <y>10</y>
    <width>150</width>
    <height>60</height>
    <font>
      <font name="GROUP-HEADER" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK-TEXT" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open Legend Action</name>
    <actions>
      <action type="open_display">
        <file>$(WIDGET_ROOT)/COMMON/legend.bob</file>
        <target>standalone</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>Open
Legend</text>
    <x>1500</x>
    <y>10</y>
    <width>150</width>
    <height>60</height>
    <font>
      <font name="GROUP-HEADER" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK-TEXT" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe Main</name>
    <y>350</y>
    <width>1800</width>
    <height>1</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="1800.0" y="0.0">
      </point>
    </points>
  </widget>
  <widget type="group" version="2.0.0">
    <name>$(SEC)-$(SECTOR)</name>
    <macros>
      <SECTOR>01</SECTOR>
    </macros>
    <y>100</y>
    <height>500</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="polyline" version="2.0.0">
      <name>$(SEC)-$(SECTOR) Separator</name>
      <width>300</width>
      <height>500</height>
      <line_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </line_color>
      <line_style>1</line_style>
      <points>
        <point x="300.0" y="0.0">
        </point>
        <point x="300.0" y="500.0">
        </point>
      </points>
    </widget>
    <widget type="label" version="2.0.0">
      <name>$(SEC)-$(SECTOR)</name>
      <text>$(SEC)-$(SECTOR)</text>
      <x>70</x>
      <y>50</y>
      <width>160</width>
      <height>50</height>
      <font>
        <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
        </font>
      </font>
      <foreground_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Pipe MEBT-$(SECTOR)</name>
      <x>75</x>
      <y>210</y>
      <width>133</width>
      <height>100</height>
      <points>
        <point x="71.0" y="0.0">
        </point>
        <point x="71.0" y="40.0">
        </point>
        <point x="91.0" y="40.0">
        </point>
        <point x="91.0" y="80.0">
        </point>
      </points>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vgc/vac_vgc.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-10001</CONTROLLER>
        <DEV>VGC</DEV>
        <IDX>10000</IDX>
        <PIPE_BELOW>true</PIPE_BELOW>
        <RELAY1_DESC>Interlock PLC: Gates Valves Interlock (MPS)</RELAY1_DESC>
        <RELAY2_DESC>Process PLC: Not used</RELAY2_DESC>
        <RELAY3_DESC>Process PLC: Not used</RELAY3_DESC>
        <RELAY4_DESC>&lt;not wired&gt;</RELAY4_DESC>
      </macros>
      <x>75</x>
      <y>140</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vpi/vac_vpi.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPI-10001</CONTROLLER>
        <DEV>VPI</DEV>
        <IDX>10000</IDX>
        <PIPE_ABOVE>true</PIPE_ABOVE>
        <PIPE_BELOW>false</PIPE_BELOW>
      </macros>
      <x>95</x>
      <y>290</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>$(SEC)-$(SECTOR)</name>
    <macros>
      <SECTOR>02</SECTOR>
    </macros>
    <x>300</x>
    <y>100</y>
    <height>500</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="polyline" version="2.0.0">
      <name>$(SEC)-$(SECTOR) Separator</name>
      <width>300</width>
      <height>500</height>
      <line_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </line_color>
      <line_style>1</line_style>
      <points>
        <point x="300.0" y="0.0">
        </point>
        <point x="300.0" y="500.0">
        </point>
      </points>
    </widget>
    <widget type="label" version="2.0.0">
      <name>$(SEC)-$(SECTOR)</name>
      <text>$(SEC)-$(SECTOR)</text>
      <x>70</x>
      <y>50</y>
      <width>160</width>
      <height>50</height>
      <font>
        <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
        </font>
      </font>
      <foreground_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Pipe MEBT-$(SECTOR)</name>
      <x>75</x>
      <y>210</y>
      <width>133</width>
      <height>100</height>
      <points>
        <point x="71.0" y="0.0">
        </point>
        <point x="71.0" y="40.0">
        </point>
        <point x="91.0" y="40.0">
        </point>
        <point x="91.0" y="80.0">
        </point>
      </points>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vgp/vac_vgp.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-10001</CONTROLLER>
        <DEV>VGP</DEV>
        <IDX>20000</IDX>
        <PIPE_BELOW>true</PIPE_BELOW>
        <RELAY1_DESC>Process PLC: Atmospheric pressure</RELAY1_DESC>
        <RELAY2_DESC>Process PLC: "Vacuum" -&gt; Threshold to start the VPTs</RELAY2_DESC>
      </macros>
      <x>75</x>
      <y>140</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vpi/vac_vpi.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPI-10001</CONTROLLER>
        <DEV>VPI</DEV>
        <IDX>20000</IDX>
        <PIPE_ABOVE>true</PIPE_ABOVE>
        <PIPE_BELOW>false</PIPE_BELOW>
      </macros>
      <x>95</x>
      <y>290</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>$(SEC)-$(SECTOR)</name>
    <macros>
      <SECTOR>03</SECTOR>
    </macros>
    <x>600</x>
    <y>100</y>
    <width>600</width>
    <height>950</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="polyline" version="2.0.0">
      <name>$(SEC)-$(SECTOR) Separator</name>
      <width>600</width>
      <height>500</height>
      <line_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </line_color>
      <line_style>1</line_style>
      <points>
        <point x="600.0" y="0.0">
        </point>
        <point x="600.0" y="500.0">
        </point>
      </points>
    </widget>
    <widget type="label" version="2.0.0">
      <name>$(SEC)-$(SECTOR)</name>
      <text>$(SEC)-$(SECTOR)</text>
      <x>210</x>
      <y>50</y>
      <width>160</width>
      <height>50</height>
      <font>
        <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
        </font>
      </font>
      <foreground_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Pipe VPI</name>
      <x>75</x>
      <y>250</y>
      <width>133</width>
      <height>40</height>
      <points>
        <point x="71.0" y="0.0">
        </point>
        <point x="71.0" y="40.0">
        </point>
      </points>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vpi/vac_vpi.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPI-20001</CONTROLLER>
        <DEV>VPI</DEV>
        <IDX>50000</IDX>
        <PIPE_ABOVE>true</PIPE_ABOVE>
        <PIPE_BELOW>false</PIPE_BELOW>
      </macros>
      <x>75</x>
      <y>290</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Pipe $(SEC)-$(SECTOR)</name>
      <x>375</x>
      <y>210</y>
      <width>133</width>
      <height>520</height>
      <points>
        <point x="91.0" y="0.0">
        </point>
        <point x="91.0" y="40.0">
        </point>
        <point x="71.0" y="40.0">
        </point>
        <point x="71.0" y="520.0">
        </point>
      </points>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vbd/vac_vbd.bob</file>
      <macros>
        <DEV>VBD</DEV>
        <IDX>10000</IDX>
      </macros>
      <x>395</x>
      <y>140</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vvg/vac_vvg.bob</file>
      <macros>
        <DEV>VVG</DEV>
        <IDX>01100</IDX>
        <PIPE_VERTICAL>true</PIPE_VERTICAL>
        <ROTATION>270</ROTATION>
      </macros>
      <x>375</x>
      <y>290</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Pipe Venting</name>
      <x>343</x>
      <y>500</y>
      <width>103</width>
      <height>70</height>
      <points>
        <point x="0.0" y="25.0">
        </point>
        <point x="103.0" y="25.0">
        </point>
      </points>
    </widget>
    <widget type="group" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DEV)-$(IDX) Venting</name>
      <macros>
        <DEV>VVM</DEV>
        <IDX>01100</IDX>
      </macros>
      <x>210</x>
      <y>500</y>
      <width>133</width>
      <height>88</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="embedded" version="2.0.0">
        <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
        <file>$(WIDGET_ROOT)/vvm/vac_angle-vvm.bob</file>
        <macros>
          <PIPE_RIGHT>true</PIPE_RIGHT>
        </macros>
        <width>131</width>
        <height>68</height>
        <resize>2</resize>
      </widget>
      <widget type="label" version="2.0.0">
        <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX) Venting</name>
        <text>Venting</text>
        <x>46</x>
        <y>72</y>
        <width>51</width>
        <height>16</height>
        <font>
          <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
          </font>
        </font>
        <horizontal_alignment>1</horizontal_alignment>
        <auto_size>true</auto_size>
      </widget>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vgc/vac_vgc.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-01100</CONTROLLER>
        <DEV>VGC</DEV>
        <IDX>01100</IDX>
        <PIPE_LEFT>true</PIPE_LEFT>
        <RELAY1_DESC>&lt;not wired&gt;</RELAY1_DESC>
        <RELAY2_DESC>Process PLC: Not used</RELAY2_DESC>
        <RELAY3_DESC>Process PLC: Not used</RELAY3_DESC>
        <RELAY4_DESC>&lt;not wired&gt;</RELAY4_DESC>
      </macros>
      <x>448</x>
      <y>560</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vgp/vac_vgp.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-01100</CONTROLLER>
        <DEV>VGP</DEV>
        <IDX>01100</IDX>
        <PIPE_RIGHT>true</PIPE_RIGHT>
        <RELAY1_DESC>Process PLC: Atmospheric pressure</RELAY1_DESC>
        <RELAY2_DESC>Process PLC: Not used</RELAY2_DESC>
      </macros>
      <x>311</x>
      <y>630</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-VPT-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vpt/vac_vpt.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPT-$(IDX)</CONTROLLER>
        <DEV>VPT</DEV>
        <IDX>01100</IDX>
        <PIPE_ABOVE>true</PIPE_ABOVE>
        <PIPE_LEFT>true</PIPE_LEFT>
      </macros>
      <x>375</x>
      <y>730</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Pipe $(SEC)-$(SUBSEC) Pumps</name>
      <x>150</x>
      <y>730</y>
      <width>225</width>
      <height>110</height>
      <points>
        <point x="225.0" y="25.0">
        </point>
        <point x="71.0" y="25.0">
        </point>
        <point x="71.0" y="110.0">
        </point>
      </points>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vva/vac_angle-vva.bob</file>
      <macros>
        <DEV>VVA</DEV>
        <IDX>00011</IDX>
        <PIPE_BELOW>true</PIPE_BELOW>
        <PIPE_RIGHT>true</PIPE_RIGHT>
        <ROTATION>0</ROTATION>
      </macros>
      <x>150</x>
      <y>730</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vpp-vpdp/vac_vpp-vpdp.bob</file>
      <macros>
        <DEV>VPDP</DEV>
        <IDX>00011</IDX>
        <PIPE_ABOVE>true</PIPE_ABOVE>
      </macros>
      <x>150</x>
      <y>840</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Pipe Leak Testing</name>
      <x>330</x>
      <y>730</y>
      <width>45</width>
      <height>170</height>
      <points>
        <point x="0.0" y="25.0">
        </point>
        <point x="0.0" y="125.0">
        </point>
        <point x="45.0" y="125.0">
        </point>
      </points>
    </widget>
    <widget type="group" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX) Leak Testing</name>
      <macros>
        <DEV>VVM</DEV>
        <IDX>00011</IDX>
      </macros>
      <x>375</x>
      <y>830</y>
      <width>133</width>
      <height>87</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="embedded" version="2.0.0">
        <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
        <file>$(WIDGET_ROOT)/vvm/vac_angle-vvm.bob</file>
        <macros>
          <PIPE_LEFT>true</PIPE_LEFT>
          <ROTATION>90</ROTATION>
        </macros>
        <width>131</width>
        <height>68</height>
        <resize>2</resize>
      </widget>
      <widget type="label" version="2.0.0">
        <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX) Leak Testing</name>
        <text>Leak Testing</text>
        <x>28</x>
        <y>71</y>
        <width>87</width>
        <height>16</height>
        <font>
          <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
          </font>
        </font>
        <horizontal_alignment>1</horizontal_alignment>
        <auto_size>true</auto_size>
      </widget>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>$(SEC)-$(SECTOR)</name>
    <macros>
      <SECTOR>04</SECTOR>
    </macros>
    <x>1200</x>
    <y>100</y>
    <height>500</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="polyline" version="2.0.0">
      <name>$(SEC)-$(SECTOR) Separator</name>
      <width>300</width>
      <height>500</height>
      <line_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </line_color>
      <line_style>1</line_style>
      <points>
        <point x="300.0" y="0.0">
        </point>
        <point x="300.0" y="500.0">
        </point>
      </points>
    </widget>
    <widget type="label" version="2.0.0">
      <name>$(SEC)-$(SECTOR)</name>
      <text>$(SEC)-$(SECTOR)</text>
      <x>70</x>
      <y>50</y>
      <width>160</width>
      <height>50</height>
      <font>
        <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
        </font>
      </font>
      <foreground_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Pipe $(SEC)-$(SECTOR)</name>
      <x>75</x>
      <y>210</y>
      <width>133</width>
      <height>100</height>
      <points>
        <point x="71.0" y="0.0">
        </point>
        <point x="71.0" y="40.0">
        </point>
        <point x="91.0" y="40.0">
        </point>
        <point x="91.0" y="80.0">
        </point>
      </points>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vgc/vac_vgc.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-10001</CONTROLLER>
        <DEV>VGC</DEV>
        <IDX>30000</IDX>
        <PIPE_BELOW>true</PIPE_BELOW>
        <RELAY1_DESC>&lt;not wired&gt;</RELAY1_DESC>
        <RELAY2_DESC>Process PLC: Not used</RELAY2_DESC>
        <RELAY3_DESC>Process PLC: Not used</RELAY3_DESC>
        <RELAY4_DESC>&lt;not wired&gt;</RELAY4_DESC>
      </macros>
      <x>75</x>
      <y>140</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vpi/vac_vpi.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPI-10001</CONTROLLER>
        <DEV>VPI</DEV>
        <IDX>30000</IDX>
        <PIPE_ABOVE>true</PIPE_ABOVE>
        <PIPE_BELOW>false</PIPE_BELOW>
      </macros>
      <x>95</x>
      <y>290</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>$(SEC)-$(SECTOR)</name>
    <macros>
      <SECTOR>05</SECTOR>
    </macros>
    <x>1500</x>
    <y>100</y>
    <height>500</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="polyline" version="2.0.0">
      <name>$(SEC)-$(SECTOR) Separator</name>
      <width>300</width>
      <height>500</height>
      <line_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </line_color>
      <line_style>1</line_style>
      <points>
        <point x="300.0" y="0.0">
        </point>
        <point x="300.0" y="500.0">
        </point>
      </points>
    </widget>
    <widget type="label" version="2.0.0">
      <name>$(SEC)-$(SECTOR)</name>
      <text>$(SEC)-$(SECTOR)</text>
      <x>70</x>
      <y>50</y>
      <width>160</width>
      <height>50</height>
      <font>
        <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
        </font>
      </font>
      <foreground_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Pipe $(SEC)-$(SECTOR)</name>
      <x>75</x>
      <y>210</y>
      <width>133</width>
      <height>100</height>
      <points>
        <point x="71.0" y="0.0">
        </point>
        <point x="71.0" y="40.0">
        </point>
        <point x="91.0" y="40.0">
        </point>
        <point x="91.0" y="80.0">
        </point>
      </points>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vgc/vac_vgc.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-10002</CONTROLLER>
        <DEV>VGC</DEV>
        <IDX>40000</IDX>
        <PIPE_BELOW>true</PIPE_BELOW>
        <RELAY1_DESC>Interlock PLC: Gates Valves Interlock (MPS)</RELAY1_DESC>
        <RELAY2_DESC>Process PLC: Not used</RELAY2_DESC>
        <RELAY3_DESC>Process PLC: Not used</RELAY3_DESC>
        <RELAY4_DESC>&lt;not wired&gt;</RELAY4_DESC>
      </macros>
      <x>75</x>
      <y>140</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
      <file>$(WIDGET_ROOT)/vpi/vac_vpi.bob</file>
      <macros>
        <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPI-10001</CONTROLLER>
        <DEV>VPI</DEV>
        <IDX>40000</IDX>
        <PIPE_ABOVE>true</PIPE_ABOVE>
        <PIPE_BELOW>false</PIPE_BELOW>
      </macros>
      <x>95</x>
      <y>290</y>
      <width>131</width>
      <height>68</height>
      <resize>2</resize>
    </widget>
  </widget>
</display>
