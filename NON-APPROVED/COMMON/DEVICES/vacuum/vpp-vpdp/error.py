#Generated from VACUUM_VAC-VPP_VAC-VPDP.def at 2020-06-11_16:17:53
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

msg  = ""
code = 0

if PVUtil.getLong(pvs[0]):
    code = PVUtil.getLong(pvs[1])

    msgs = dict({
                 99 : "Controller Error (Hardware Error)",
                 98 : "Pressure Interlock",
                 97 : "Hardware Interlock",
                 96 : "Software Interlock",
                 95 : "Circuit Breaker Tripped",
                 49 : "Controller Error (Hardware Error) - Auto Reset",
                 48 : "Pressure Interlock - Auto Reset",
                 47 : "Hardware Interlock - Auto Reset",
                 46 : "Software Interlock - Auto Reset",
                 2 : 'Pump Disconnected - Auto Reset (for Primary Pump Controller Type "Hilko Spoelstra")',
                 1 : 'Local Control - Auto Reset (for Primary Pump Controller Type "Hilko Spoelstra")',
                 0 : ""
                })

    try:
        msg = msgs[code]
    except KeyError:
        msg = "Error Code: " + PVUtil.getString(pvs[1])
        ScriptUtil.getLogger().severe("Unknown error code {} : {}".format(pvs[1], code))

try:
    pvs[2].setValue(msg)
except:
    if widget.getType() != "action_button":
        widget.setPropertyValue("text", msg)
    widget.setPropertyValue("tooltip", msg)
