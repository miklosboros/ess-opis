from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil

tooltip  = "N/A"

try:
    pvStat   = PVUtil.getString(pvs[0]).upper()
    pvPrsStr = PVUtil.getString(pvs[2])

    if pvStat == "OPEN":
        try:
            from org.phoebus.ui.vtype import FormatOption, FormatOptionHandler
        except ImportError:
            try:
                from org.csstudio.display.builder.model.properties import FormatOption
                from org.csstudio.display.builder.model.util import FormatOptionHandler
            except Exception as e:
                ScriptUtil.getLogger().severe(str(e))

        try:
            vtype = PVUtil.getVType(pvs[1])

            tooltip = FormatOptionHandler.format(vtype, FormatOption.EXPONENTIAL, 2, True)
        except:
            pvUnit  = PVUtil.getString(pvs[3])
            tooltip = pvPrsStr + " " + pvUnit
    else:
        tooltip = pvPrsStr
except Exception as e:
    ScriptUtil.getLogger().severe(str(e))

widget.setPropertyValue("tooltip", tooltip)
