PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var pvOpen      = 0;

var pvSymbol    = pvs[0];

var colorID = 0;

var debug = widget.getEffectiveMacros().getValue("DEBUG");
if (debug) {
	debug = debug[0];
	switch (debug) {
		case '1':
		case 'Y':
		case 'y':
		case 'T':
		case 't':
			debug = true;
			break;

		default:
			debug = false;
	}
}
else
	debug = false;

if (debug)
	Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();
else {
	Logger = new Object();
	Logger.info = function() {}
	Logger.warning = function() {}
	Logger.severe = function(text) { org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe(text);}
}

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvOpen      = PVUtil.getInt(pvs[1]);

	log_pv(pvs[1]);

	if (pvOpen) {
		Logger.info(pvSymbol + ": OPEN");
		colorID = 1;
	} else {
		Logger.info(pvSymbol + ": CLOSED");
		colorID = 2;
	}
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}


pvSymbol.write(colorID);
